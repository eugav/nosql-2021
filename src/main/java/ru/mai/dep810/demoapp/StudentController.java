package ru.mai.dep810.demoapp;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.mai.dep810.demoapp.model.Address;
import ru.mai.dep810.demoapp.model.Student;
import ru.mai.dep810.demoapp.repository.StudentRepository;

@RestController
public class StudentController {

    private StudentRepository studentRepository;

    public StudentController(@Qualifier("hazelcastCachedStudentRepository") StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    private final List<Student> students = new ArrayList<>(List.of(
            new Student("1", "John Smith", 21, List.of(new Address("RU", "Moscow", "1st Volokolamkiy pr"))),
            new Student("2", "Steve Howking", 25, List.of(new Address("UK", "London", "Liverpool st")))
    ));

    @GetMapping("/student")
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @GetMapping("/student/{id}")
    public Student getStudentById(@PathVariable("id") String id) {
        return studentRepository.findById(id);
    }

    @PostMapping("/student")
    public Student addStudent(@RequestBody Student student) {
        return studentRepository.save(student);
    }

    @PutMapping("/student/{id}")
    public Student updateStudent(@PathVariable("id")String id, @RequestBody Student student) {
        Student existing = studentRepository.findById(id);
        existing.setAddresses(student.getAddresses());
        existing.setAge(student.getAge());
        existing.setName(student.getName());
        return studentRepository.save(existing);
    }

    @DeleteMapping("/student/{id}")
    public void deleteStudent(@PathVariable("id") String id) {
        studentRepository.delete(id);
    }
}
