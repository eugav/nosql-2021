package ru.mai.dep810.demoapp.repository;

import java.util.List;
import ru.mai.dep810.demoapp.model.Student;

public interface StudentRepository {
    List<Student> findAll();

    Student findById(String id);

    Student save(Student student);

    void delete(String id);
}
