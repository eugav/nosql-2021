package ru.mai.dep810.demoapp.model;

import java.io.Serializable;
import java.util.List;

public class Student implements Serializable {

    private static final long serialVersionUID = 3188156933445538333L;

    private String id;
    private String name;
    private Integer age;
    private List<Address> addresses;


    public Student() {
    }

    public Student(String id, String name, Integer age, List<Address> addresses) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.addresses = addresses;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }
}
