package ru.mai.dep810.demoapp.model;

public class Post {
    private String id;
    private String body;

    public Post() {
    }

    public Post(String id, String body) {
        this.id = id;
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
